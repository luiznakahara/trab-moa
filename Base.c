
/**
 estrutura que representa os
possiveis estados do tabuleiro do jogo
	 ___________________________
	|     1 |    2  |  6  | 8   |
        _____________________________
        |     4 |  3    |   7 | 9   |
        _____________________________
        |     5 |  11   | 10  |  13 |
        _____________________________
        |   12  |    14 | 15  |     |
        _____________________________
  
sendo que
 pai: estado anterior
 coordernaXpecaBranca e
 coordernaYpecaBranca; e a posicao
 da peca branca no tabuleiro do estado atual
*/
struct No {
   int matriz[][];
   int *pai;
   int coordernaXpecaBranca;
   int coordernaYpecaBranca;
}
