
package algoritimos;

import estrutura.No;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author fernando
 */
public class utilTest {
    
    public utilTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of geraMovimentos method, of class util.
     */
    @Test
    public void buscaRespostaTest() {
         System.out.println("buscaResposta");
 
         
        
        
        String[][] estadoIncial3 = {{"1","12","11","10"}, //caso base
                                    {"2","13","#","9"},
                                    {"3","14","15","8"},
                                    {"4","5","6","7"}};
        No raiz03 = new No(estadoIncial3, 1, 2, null);
        
        String[][] estadoIncial2 =  {{"1","12","11","10"}, //um movimento
                                     {"2","13","15","9"},
                                     {"3","14","#","8"},
                                     {"4","5","6","7"}};
        No raiz02 = new No(estadoIncial2, 2, 2, null);
         
        String[][] estadoIncial1 = {{"1", "12", "11" ,"10"}, //Caso01
                                    {"#", "2", "13", "8"},
                                    {"5", "4", "9", "15"},
                                    {"3", "6", "14", "7"}};
         

        No raiz01 = new No(estadoIncial1, 1, 0, null);
         
        
        
        String[][] estadoIncial4 = {{"1", "9", "10", "11" }, //Caso02
                                    {"12", "3", "13", "8"},
                                    {"4", "15", "6", "14" },
                                    {"5", "2", "#", "7"}};
        No raiz04 = new No(estadoIncial4, 3, 2, null);
        
        
        String[][] estadoIncial5 ={{"2", "13", "3", "11"}, //Caso03
                                    {"5", "15", "14", "9"}, 
                                    {"4", "10", "8", "#"}, 
                                    {"6", "1", "12", "7"}};
        No raiz05=  new No(estadoIncial5,2, 3, null);
        
        
        String[][] estadoIncial6 ={{"1", "11", "10", "15"}, //Caso04 
                                   {"3", "8", "13", "6"}, 
                                   {"12", "#", "4", "5"}, 
                                   {"2", "9", "14", "7"}};
        No raiz06 =  new No(estadoIncial6,2, 1, null);
        
        
        String[][] estadoIncial7 ={{"9", "5", "11", "#"}, //Caso05 
                                   {"12", "3", "14", "6"}, 
                                   {"8", "15", "13", "4"}, 
                                   {"10", "2", "7", "1"}};
        No raiz07=  new No(estadoIncial7,0, 3, null);
         
        util instance3 = new util();
        assertEquals(instance3.buscaResposta(raiz03),true);
        System.out.println("__________________Resolve: Caso Base -> tabuleiro resolvido___________________________________________________________||||");
        System.out.println("----entrada:--------");
        imprimeEntrada(estadoIncial3);
        System.out.println("");
        System.out.println("---------Saida:----------");
        imprimeResposta(instance3);
        System.out.println("__________________________________________________________________________________||||");
        
        util instance2 = new util();
        assertEquals(instance2.buscaResposta(raiz02),true);
        System.out.println("||||____________Resolve: em um movimento___________________________________________________________||||");
        System.out.println("----entrada:--------");
        imprimeEntrada(estadoIncial2);
        System.out.println("");
        System.out.println("---------Saida:----------");
        imprimeResposta(instance2);
        System.out.println("||||______________________________________________________________________________||||");
        
        util instance1 = new util();
        assertEquals(instance1.buscaResposta(raiz01),true);
        System.out.println("||||____________Resolve: caso 01___________________________________________________________||||");
        System.out.println("----entrada:--------");
        imprimeEntrada(estadoIncial1);
        System.out.println("");
        System.out.println("---------Saida:----------");
        imprimeResposta(instance1);
        System.out.println("||||______________________________________________________________________________||||");
         
        util instance4 = new util();
        assertEquals(instance4.buscaResposta(raiz04),true);
        System.out.println("________________Resolve: caso 02___________________________________________________________||||");
        System.out.println("----entrada:--------");
        imprimeEntrada(estadoIncial4);
        System.out.println("");
        System.out.println("---------Saida:----------");
        imprimeResposta(instance4);
        System.out.println("__________________________________________________________________________________||||");
        
        util instance5 = new util();
        assertEquals(instance5.buscaResposta(raiz05),true);
        System.out.println("___________________Resolve: caso 03___________________________________________________________||||");
        System.out.println("----entrada:--------");
        imprimeEntrada(estadoIncial5);
        System.out.println("");
        System.out.println("---------Saida:----------");
        imprimeResposta(instance5);
        System.out.println("___________________________________________________________________________________||||");
        
        util instance6 = new util();
        assertEquals(instance6.buscaResposta(raiz06),true);
        System.out.println("___________________Resolve: caso 04___________________________________________________________||||");
        System.out.println("----entrada:--------");
        imprimeEntrada(estadoIncial6);
        System.out.println("");
        System.out.println("---------Saida:----------");
        imprimeResposta(instance6);
        System.out.println("___________________________________________________________________________________||||");
        
        util instance7 = new util();
        assertEquals(instance7.buscaResposta(raiz07),true);
        System.out.println("___________________Resolve: caso 05___________________________________________________________||||");
        System.out.println("----entrada:--------");
        imprimeEntrada(estadoIncial7);
        System.out.println("");
        System.out.println("---------Saida:----------");
        imprimeResposta(instance7);
        System.out.println("___________________________________________________________________________________||||");
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    public void imprimeResposta(util solucao){
        int altura = 0;
        for (No no : solucao.cjSolucao()) {
            
            for(String[] linha : no.getEstadoAtual()){
                for(String item : linha){
                    //System.out.print("|"+item+"|");
                }
                //System.out.println("");
            }
            
            altura++;
            //System.out.println("|---------------------|");
        }
        for(String[] linha : solucao.solucao.getEstadoAtual()){
                for(String item : linha){
                    System.out.print("|"+item+"|");
                }
                System.out.println("");
            }
        System.out.println("Numero Movimentos: "+altura);
    }
    
    public void imprimeEntrada(String[][] in){
        for(String[] linha : in){
            for(String item : linha){
                System.out.print("|"+item+"|");
            }
            System.out.println("");
        }
        
    }
    
}
