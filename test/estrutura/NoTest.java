package estrutura;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author fernando
 * testes dos metdos da classe no
 */
public class NoTest {
    
    
    String[][] estadoInicia2 =  {{"1","12","11","10"}, //caso base
                                 {"2","13","#","9"},
                                 {"3","14","15","8"},
                                 {"4","5","6","7"}};
    
    String[][] estadoIncial = { {"1", "12", "11" ,"10"}, //Caso01
                                {"#", "2", "13", "8"},
                                {"5", "4", "9", "15"},
                                {"3", "6", "14", "7"}};
    
    String[][] estadoInicia3 =  {{"1", "9", "10", "11" }, //Caso02
                                 {"12", "3", "13", "8"},
                                 {"4", "15", "6", "14" },
                                 {"5", "2", "#", "7"}};
                                 
    String[][] estadoInicia4 =  {{"2", "13", "3", "11"}, //Caso03
                                 {"5", "15", "14", "9"}, 
                                 {"4", "10", "8", "#"}, 
                                 {"6", "1", "12", "7"}};
    
    String[][] estadoInicia5 =  {  {"1", "11", "10", "15"}, //Caso04 
                                   {"3", "8", "13", "6"}, 
                                   {"12", "#", "4", "5"}, 
                                   {"2", "9", "14", "7"}};
    
    String[][] estadoInicia6 =  {  {"9", "5", "11", "#"}, //Caso05 
                                   {"12", "3", "14", "6"}, 
                                   {"8", "15", "13", "4"}, 
                                   {"10", "2", "7", "1"}};
    No instance;
    
    No instance02;
    
    No instance03;
    
    No instance04;
    
    No instance05;
    
    No instance06;
    
    public NoTest() {
        this.instance = new No(estadoIncial, 1, 0, null);
        this.instance02 = new No(estadoInicia2, 1, 2, null);
        this.instance03 = new No(estadoInicia3, 3, 2, null);
        this.instance04 = new No(estadoInicia4, 2, 1, null);
        this.instance05 = new No(estadoInicia5, 3, 0, null);
        this.instance06 = new No(estadoInicia6, 0, 3, null);
    }

    /**
     * verica o estado é o
     * esperado.
     */
    @Test
    public void testChecaEstado() {
        System.out.println("checa o estado");
        instance.calculaPecaForaLugar();
        assertEquals(instance.checaEstado(), false);
        instance02.calculaPecaForaLugar();
        assertEquals(instance02.checaEstado(), true);
        instance03.calculaPecaForaLugar();
        assertEquals(instance03.checaEstado(), false);
        instance04.calculaPecaForaLugar();
        assertEquals(instance04.checaEstado(), false);
        instance05.calculaPecaForaLugar();
        assertEquals(instance05.checaEstado(), false);
    }
    
    /**
     * verifica se a peca pode ser movida para
     * a posicao desejada
     */
    @Test
    public void testPosicaoValida(){
        System.out.println("posicao valida");
        //x = 1 y = 1
        assertEquals(instance.checaUp(), true);
        assertEquals(instance.checaDown(), true);
        assertEquals(instance.checaleft(), false);
        assertEquals(instance.checaRight(),true);
        
        //x = 3 y = 3
        assertEquals(instance02.checaUp(), true);
        assertEquals(instance02.checaDown(), true);
        assertEquals(instance02.checaleft(), true);
        assertEquals(instance02.checaRight(),true);
        
        //x = 0 y = 0
        assertEquals(instance03.checaUp(), true);
        assertEquals(instance03.checaDown(), false);
        assertEquals(instance03.checaleft(), true);
        assertEquals(instance03.checaRight(),true);
        
        //x = 0 y = 3
        assertEquals(instance04.checaUp(), true);
        assertEquals(instance04.checaDown(), true);
        assertEquals(instance04.checaleft(), true);
        assertEquals(instance04.checaRight(),true);
        
        //x = 3 y = 0
        assertEquals(instance05.checaUp(), true);
        assertEquals(instance05.checaDown(), false);
        assertEquals(instance05.checaleft(), false);
        assertEquals(instance05.checaRight(),true);
    }

    /**
     * testa se a peca esta se movendo para a posicao
     * passada por parametro.
     */
    @Test
    public void testMovePeca() {
        System.out.println("movePeca");
        instance.moveDireita();
        assertEquals(instance.getPecaAtual(),"#");
        assertEquals(instance.getPeca(1, 0), "2");
        instance06.moveBaixo();
        assertEquals(instance06.getPecaAtual(),"#");
        assertEquals(instance06.getPeca(0, 3), "6");
    }
    
    @Test
    public void testCalculaChave(){
        System.out.println("CalculaChave");
        this.instance02.calculaPecaForaLugar();
        assertEquals(instance02.getQntPecaForaLugar(), 0);
    }
    
    @Test
    public void testChaveMap(){
        System.out.println("testa o chave do map");
        assertEquals(this.instance06.criaChaveMap(), "9511#12314681513410271");
        System.out.println(instance06.criaChaveMap());
    }
    
    @Test
    public void testDistPosFinal(){
       System.out.println("testa a distancias dos elmentos ate sua posicao final"); 
        assertEquals(this.instance02.calcDistPosFinal(0,0), 0);
        //assertEquals(this.instance04.calcDistPosFinal(0,0), 4);
        //assertEquals(this.instance06.calcDistPosFinal(0,0), 7);
        assertEquals(this.instance03.calcDistPosFinal(0,2), 1);
        assertEquals(this.instance.calcDistPosFinal(1,1), 1);
    }
    
    @Test
    public void testSomaDist(){
        System.out.println("testa a soma de todas as distancias");
        assertEquals(this.instance02.somaDistancias(), 0);
    }

}
