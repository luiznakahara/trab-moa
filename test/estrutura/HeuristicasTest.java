/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package estrutura;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luiz
 */
public class HeuristicasTest {

    public HeuristicasTest() {
    }
    @Test 
    public void testEspiral(){ 
        System.out.println("testa a espiral da matriz");
        String[][] estadoCasoBase = {{"1","12","11","10"}, //caso base 
                                     {"2","13","0","9"}, 
                                     {"3","14","15","8"}, 
                                     {"4","5","6","7"}};
                
        String[][] estadoCaso01 = { {"1", "12", "11" ,"10"}, //Caso01 
                                    {"0", "2", "13", "8"}, 
                                    {"5", "4", "9", "15"}, 
                                    {"3", "6", "14", "7"}}; 
        Heuristicas instance = new Heuristicas(); 
        assertEquals(instance.calculaPecasForaDaSequencia(estadoCasoBase), 1);       
        assertEquals(instance.calculaPecasForaDaSequencia(estadoCaso01), 13); 
    }
    
    @Test
    public void testVerificaSequencia(){
        System.out.println("Teste Verifica Sequencia");
        String[][] estadoCaso01 = { {"1", "12", "11" ,"10"}, //Caso01 
                                    {"0", "2", "13", "8"}, 
                                    {"5", "4", "9", "15"}, 
                                    {"3", "6", "14", "7"}};
        Heuristicas inst = new Heuristicas();
        assertEquals(inst.verificaSequencia("1", "1"), 1);
        assertEquals(inst.verificaSequencia("1", "2"), 0);
        assertEquals(inst.verificaSequencia("3", "5"), 1);
        assertEquals(inst.verificaSequencia(estadoCaso01[0][0], estadoCaso01[1][0]), 1);
    }
    
}
