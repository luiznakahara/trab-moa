package estrutura;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author fernando
 * conjunto de testes os metedos 
 * da estrutura de arvore
 */
public class MovimentosTest {
    
    public MovimentosTest() {
    }
    
    /**
     * verfica se os nos estao sendo gerados
     * corretamente, gera o primeiro nivel
     * da arvore apartir da raiz
     */
    @Test
    public void testGeraFilhos() {
        System.out.println("geraFilhos");
        String[][] estadoIncial =  {{"1","2","3","4"}, //nao funciona
                                   {"5","#","7","8"},
                                   {"9","10","11","12"},
                                   {"13","14","15","6"}};
        No no = new No(estadoIncial, 1, 1, null);
        Movimentos mov = new Movimentos();
        HashMap<String,No> result = mov.geraMovimentosPossiveis(no);
       for (Map.Entry<String, No> entry : result.entrySet()) {
            String string = entry.getKey();
            No no1 = entry.getValue();
            String[][] estado = no1.copiaEstadoAtual();
            for (String[] strings : estado) {
                for (String string1 : strings) {
                    System.out.print("|"+string1+"|");
                }
                System.out.println("");
           }
           System.out.println("---------------");
        }
        
        assertEquals(mov.descendente(no), true);
        assertEquals(mov.checaRepetido(), true);
        assertEquals(mov.numeroDeNos(), 4);
        
        
    }
    
}
