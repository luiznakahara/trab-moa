/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package estrutura;

import java.util.Comparator;

/**
 *
 * @author fernando
 */
public class ComparatorQntPecaForaDoLugar implements Comparator<No>{

    @Override
    public int compare(No x, No y) {
        // Assume neither string is null. Real code should
        // probably be more robust
        if (x.getHeuristicaPosForaDoLugar()< y.getHeuristicaPosForaDoLugar()){
            return -1;
        }
        if(x.getHeuristicaPosForaDoLugar()> y.getHeuristicaPosForaDoLugar()){
            return 1;
        }
        return 0;
    }
}
