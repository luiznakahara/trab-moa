/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package estrutura;

import static java.lang.Math.abs;

/**
 *
 * @author luiz
 */
public class Heuristicas {
    private int pecasForaDoLugar;
    private int pecasForaDaSequencia;
    private int distanciaEntrePos;
    
    public Heuristicas(){
        this.pecasForaDoLugar = 0;
        this.distanciaEntrePos = 0;
    }

    public int getPecasForaDaSequencia() {
        return pecasForaDaSequencia;
    }

    public void setPecasForaDaSequencia(int pecasForaDaSequencia) {
        this.pecasForaDaSequencia = pecasForaDaSequencia;
    }

    public int getPecasForaDoLugar() {
        return pecasForaDoLugar;
    }

    public void setPecasForaDoLugar(int pecasForaDoLugar) {
        this.pecasForaDoLugar = pecasForaDoLugar;
    }

    public int getDistanciaEntrePos() {
        return distanciaEntrePos;
    }

    public void setDistanciaEntrePos(int distanciaEntrePos) {
        this.distanciaEntrePos = distanciaEntrePos;
    }
    
    public void calculaHeuristicas( String[][] estadoAtual ){
        this.pecasForaDoLugar = 0;
        this.distanciaEntrePos= 0;
        for(int i = 0; i< estadoAtual.length; i++){
            for(int j = 0; j< estadoAtual[i].length; j++){
                if(!estadoAtual[i][j].equals(No.estadoFinal[i][j])){
                    this.pecasForaDoLugar++;
                    this.distanciaEntrePos += this.calculaDistanciaEntrePos(i, j, estadoAtual[i][j]);
                }
            }
        }
        this.pecasForaDaSequencia = this.calculaPecasForaDaSequencia(estadoAtual);
    }
    
    private int calculaDistanciaEntrePos(int coordX, int coordY, String conteudo){
        int tamanho = No.estadoFinal.length;
        int aux = 0;
        for(int i = 0; i < tamanho; i++){
            for(int j = 0; j < tamanho; j++){
                if(No.estadoFinal[i][j].equals(conteudo)){
                   return (abs(i - coordX) + abs(j - coordY));                   
                }
            }
        }
        return 0;
    }
    
    public int calculaPecasForaDaSequencia(String[][] estadoAtual){
        int i, j, k, l, contador;
        contador = 0;
        for(i = 0; i < 3; i++){
            contador += verificaSequencia(estadoAtual[i][0], estadoAtual[i + 1][0]);
        }
        for(j = 0; j < 3; j++){
            contador += verificaSequencia(estadoAtual[3][j], estadoAtual[3][j + 1]);
        }
        for(k = 3; k > 0; k--){
            contador += verificaSequencia(estadoAtual[k][3], estadoAtual[k - 1][3]);
        }
        for(l = 3; l > 1; l--){
            contador += verificaSequencia(estadoAtual[0][l], estadoAtual[0][l - 1]);
        }
        for(i = 0; i < 2; i++){
            contador += verificaSequencia(estadoAtual[i][0], estadoAtual[i + 1][0]);
        }
        contador += verificaSequencia(estadoAtual[2][1], estadoAtual[2][2]);
        contador += verificaSequencia(estadoAtual[2][2], estadoAtual[1][2]);
        return contador;
    }
    
    public int verificaSequencia(String anterior, String proximo){
        Integer anteriorN = Integer.parseInt(anterior);
        anteriorN++;
        String anteriorS = anteriorN.toString();
        if( anteriorS.equals(proximo) ){
            return 0;
        }
        return 1;
    }
    
}
