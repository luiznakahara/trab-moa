package estrutura;

import java.util.ArrayList;
import java.util.Collections;
/**
 *
 * @author fernando
 estrutura que representa a cjMovs de estados do jogo
 */
public class Movimentos {
    private ArrayList<String> closed;
    private ArrayList<No> open;
    
    public Movimentos() {
      this.closed = new ArrayList<>();
      this.open = new ArrayList<>();
    }

    private Boolean contemString(String esta){
        for(String string : this.closed){
            if ( string.equals(esta) ){
                return true;
            }
        }
        return false;
    }
    
    private void addLista(No novo){
        if (!this.contemString(novo.criaNomeNo())){
           this.closed.add(novo.criaNomeNo());
           this.open.add(novo);
        }   
    }
    
    /**
     * No -> Movimentos
    recebe como parametro um no
    e retorna uma cjMovs com todos
    nos possivel apartir dele
     * @param pai
     * @return 
     */
    public void geraMovimentosPossiveis(No pai){
        No aux, aux1, aux2, aux3;
        if(pai.checaUp()){
            aux = moveCima(pai);
            this.addLista(aux);
        }
        if(pai.checaDown()){
            aux1 = moveBaixo(pai);
            this.addLista(aux1);
        }
        if(pai.checaleft()){
            aux2 = moveEsquerda(pai);
            this.addLista(aux2);
        }
        if(pai.checaRight()){
            aux3 = moveDireita(pai);
            this.addLista(aux3);
        }
        ComparatorQntPecaForaDoLugar comp = new ComparatorQntPecaForaDoLugar();
        ComparatorDistPecaForaDoLugar comp2 = new ComparatorDistPecaForaDoLugar();
        ComparatorPecasForaDaSequencia comp3 = new ComparatorPecasForaDaSequencia();
        Collections.sort(open, comp2);
    }
   
    private No movePeca(No pai, int deslocX, int deslocY){
       No pai2 = pai.clonar();        
       No filho = new No(pai2.getEstadoAtual(), pai2.getCoordX(), pai2.getCoordY(), pai);
       filho.movePeca(pai2.getCoordX() + deslocX, pai2.getCoordY() + deslocY);
       filho.calculaHeuristicas();
       return filho;
    }
    
   private No moveCima(No pai){
       return movePeca(pai, -1, 0);
   }
   private No moveBaixo(No pai){
       return movePeca(pai, 1, 0);
   }
   private No moveEsquerda(No pai){
       return movePeca(pai, 0, -1);
   }
   private No moveDireita(No pai){
       return movePeca(pai, 0, +1);
   }

    public void buscaResposta(No start){
        start.calculaHeuristicas();
        if(start.checaEstado()){
            printCaminhoJogadas(start, 0);
            return;
        }
        No escolhido;
        this.open.add(start);
        this.closed.add(start.criaNomeNo());
        while(!this.open.isEmpty()){
            escolhido = this.open.get(0);
            this.open.remove(0);
            if(escolhido.checaEstado()){
                this.printCaminhoJogadas(escolhido, 0);
                return;
            }
            this.geraMovimentosPossiveis(escolhido);
        }
    }

    public void printCaminhoJogadas(No fimJogo, int jogadas){
        try {
            printCaminhoJogadas(fimJogo.getPai(), jogadas + 1);        
            System.out.println("Jogadas:");
            fimJogo.printNo();
        } catch (Exception e) {
            System.out.println("Fim de Jogo!");
            System.out.println( jogadas + " Jogadas Necessarias!");
            System.out.println("");
        }

    }
    
    public ArrayList<No> getOpen(){
        return this.open;
    }  
}
