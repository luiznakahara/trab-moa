package estrutura;

/**
 *
 * @author fernando
 * estrutura que reprenta um nó na arvore
 * contem uma matriz 4X4 com estado atual do jogo
 * uma matriz com resultado final esperado
 * um nó que aponta para o para o pai
 * um x e y que são as posicoes atuais de "0"
 */
public class No {
    public static final String[][] estadoFinal = {{"1","12","11","10"},
                                                  {"2","13","0","9"},
                                                  {"3","14","15","8"},
                                                  {"4","5","6","7"}};
    private String[][] estadoAtual;
    private No pai;
    private Integer coordX;
    private Integer coordY;
    private Heuristicas heuristicas;
    
    /**
     * String[][] -> No
     * cria um novo com um estado atual;
     * @param estado
     */
    public void setEstadoAtual( String[][] estado ){
        this.estadoAtual = estado;
    }
    
    public No clonar(){
        String[][] auxEstado = clonaEstadoAtual();
        No aux = new No( this.getEstadoAtual().clone(), this.getCoordX(), this.getCoordY(), this.getPai() );
        aux.setEstadoAtual(auxEstado);
        return aux;
    }
    
    public No(String[][] estado, int coordX, int coordY, No pai) {
        this.estadoAtual = ( String[][] ) estado.clone();
        this.coordX = coordX;
        this.coordY = coordY;
        this.pai = pai;
        this.heuristicas = new Heuristicas();
    }
    
    /**
     * void -> boolean
     * retorna true se estado final foi
     * alcançado
     * @return 
     */
    public Boolean checaEstado(){       
        return this.heuristicas.getPecasForaDoLugar() == 0;
    }

    public void calculaHeuristicas(){
        this.heuristicas.calculaHeuristicas(this.estadoAtual);
    }
    
    public String criaNomeNo(){
        StringBuilder buffer = new StringBuilder();
        for (String[] estadoAtual1 : this.estadoAtual) {
            for (String estadoAtual11 : estadoAtual1) {
                buffer.append(estadoAtual11);
            }
        }
        return buffer.toString();
    }
    
    /**
     * Integer, Integer -> String[][]
     * atualiza a matriz de estado atual para a 
     * posicao a b que passado como parametro 
     * @param a
     * @param b
     */
    public void movePeca(Integer novaCoordX, Integer novaCoordY){
        String troca = this.getPeca(novaCoordX, novaCoordY);
        this.estadoAtual[this.coordX][this.coordY] = troca;
        this.estadoAtual[novaCoordX][novaCoordY] = "0";        
        this.coordX = novaCoordX;
        this.coordY = novaCoordY;
    }   

    public String[][] clonaEstadoAtual(){
        String[][] buffer = new String[4][4];
        for (int x = 0; x< 4; x++) {
            for (int y = 0; y < 4; y++) {
                buffer[x][y] = (String) this.getPeca(x, y);
            }
        }
        return buffer;
    }
    
    public String getPeca(int coordX, int coordY){
        return this.estadoAtual[coordX][coordY];
    }
    
    public String getPecaEmBranco(){
        return this.estadoAtual[this.coordX][this.coordY];
    }

    public No getPai() {
        return this.pai;
    }
      
    public String[][] getEstadoAtual() {
        return estadoAtual;
    }  

    public Integer getCoordX() {
        return coordX;
    }

    public Integer getCoordY() {
        return coordY;
    }
    
    public Boolean checaUp() {
        return this.coordX > 0;
    }

    public Boolean checaDown() {
        return this.coordX < 3;
    }

    public Boolean checaleft() {
        return this.coordY > 0;
    }

    public Boolean checaRight() {
        return this.coordY < 3;
    }

    public int getHeuristicaPosForaDoLugar() {
        return this.heuristicas.getPecasForaDoLugar();
    }
    
    public int getHeuristicaDistPosForaDoLugar(){
        return this.heuristicas.getDistanciaEntrePos();
    }
    
    public int getHeuristicaPosForaDaSequencia(){
        return this.heuristicas.getPecasForaDaSequencia();
    }

    public void printNo(){
        for( String[] list : this.estadoAtual ){
            for( String elem : list){
                System.out.print(elem + " ");
            }
            System.out.println("");
        }
        System.out.println("");
    }
}
