package main;

import estrutura.Movimentos;
import estrutura.No;

/**
 *
 * @author fernando
 */
public class Main {

    public static void main(String[] args) {
        String[][] estadoInicia6 =  {{"9", "5", "11", "0"}, 
                                     {"12", "3", "14", "6"}, 
                                     {"8", "15", "13", "4"}, 
                                     {"10", "2", "7", "1"}};
        No no = new No(estadoInicia6, 0, 3, null);
        Movimentos a = new Movimentos();
        a.buscaResposta(no.clonar());
    }
 }

